import styled from "styled-components";

export const BackGroundH = styled.div`
  display: flex;

  img {
    width: 100%;
    height: 90vh;
    position: absolute;
    object-fit: cover;
    z-index: -2;
  }
`;
